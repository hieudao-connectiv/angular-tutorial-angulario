// The Angular CLI generated an "AppModule" class automatically in src/app/app.module.ts when it created the project.

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';                 // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroesComponent } from './heroes/heroes.component';
import { MessagesComponent } from './messages/messages.component';

import { AppRoutingModule } from './app-routing.module';


// The most important @NgModule decorator annotates the top-level AppModule class.
// The @NgModule metadata's imports array, which contains a list of external modules that the app needs.
// Every component must be declared in exactly one NgModule.

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,

    // the Angular CLI declared HeroesComponent automatically in the AppModule when it generated that component.
    HeroesComponent,

    HeroDetailComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    // no need to place any providers due to the `providedIn` flag...
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

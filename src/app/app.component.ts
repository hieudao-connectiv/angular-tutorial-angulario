import { Component } from '@angular/core';

// tslint:disable-next-line: max-line-length
// Some of the metadata is in the @Component decorators that you added to your component classes. Other critical metadata is in @NgModule decorators.
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Tour of Heroes';
}

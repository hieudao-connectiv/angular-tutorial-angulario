// create a 'HeroService' that all application classes can use to get heroes.

// Instead of creating that service with the 'new' keyword,
//  you'll rely on Angular dependency injection to inject it into the 'HeroesComponent' constructor.
// "Dependency injection" or DI is a design pattern in which a class requests dependencies from external sources rather than creating them.

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Hero } from './hero';
import { HEROES } from './mock-heroes';
import { MessageService } from './message.service';

// The new service imports the Angular 'Injectable' symbol and annotates the class with the @Injectable() decorator.
// This marks the class as one that participates in the dependency injection system.
// The @Injectable() decorator accepts a metadata object for the service,
//  the same way the @Component() decorator did for your component classes.
// By default, the Angular CLI command 'ng generate service' registers a provider
//  with the root injector for your service by including provider metadata, that is providedIn: 'root' in the @Injectable() decorator.
@Injectable({
  providedIn: 'root'
})

// The 'HeroService' class is going to provide an injectable service, and it can also have 'its own injected dependencies'.
export class HeroService {

  constructor(private messageService: MessageService) { }

  // Add a 'getHeroes' method to return the mock heroes.
  // getHeroes(): Hero[] {
  //   return HEROES;
  // }

  // getHeroes(): Observable<Hero[]> {
  //   const heroes = of (HEROES);
  //   return heroes;
  // }

  getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add('HeroService: fetched heroes');
    return of(HEROES);
  }

  getHero(id: number): Observable<Hero | undefined> {
    // TODO: send the message _after_ fetching the hero
    this.messageService.add(`HeroService: fetched hero id=${id}`);

  // Fix errors: https://www.reddit.com/r/Angular2/comments/juulb3/type_observablex_undefined_is_not_assignable_error/
  // Type 'Observable<Hero | undefined>' is not assignable to type 'Observable<Hero>'.
  // Type 'Hero | undefined' is not assignable to type 'Hero'.
  // Type 'undefined' is not assignable to type 'Hero'.
    return of(HEROES.find(hero => hero.id === id));
  }
}

import { MessageService } from './../message.service';
import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
// import { HEROES } from './../mock-heroes';
import { HeroService } from '../hero.service';
@Component({

  // tslint:disable-next-line: max-line-length
  // Remember that 'app-heroes' is the element selector for the HeroesComponent. So add an <app-heroes> element to the AppComponent template file
  selector: 'app-heroes',                       // the component's CSS element selector

  templateUrl: './heroes.component.html',       // the location of the component's template file.
  styleUrls: ['./heroes.component.scss']        // the location of the component's private CSS styles.
})

// Always export the component class so you can import it elsewhere ... like in the AppModule.
export class HeroesComponent implements OnInit {

  // hero = 'Windstorm';

  // hero: Hero = {
  //   id: 1,
  //   name: 'Windstorm'
  // };

  // define a component property called 'heroes' to expose the HEROES array for binding.
  // heroes = HEROES;

  // Rename the component's 'hero' property to 'selectedHero' but don't assign it. There is no selected hero when the application starts.
  // selectedHero?: Hero;

  heroes: Hero[] = [];

  // Inject the HeroService
  // Add a private heroService parameter of type HeroService to the constructor.
  // The parameter simultaneously defines a private 'heroService' property
  //  and identifies it as a HeroService injection site. (như là chỗ tiêm từ HeroService vào HerosComponent )
  // When Angular creates a HeroesComponent,
  //  the Dependency Injection system sets the heroService parameter to the singleton instance of HeroService.
  // (*) While you could call getHeroes() in the constructor, that's not the best practice.
  constructor(private heroService: HeroService, private messageService: MessageService) { }

  // lifecycle hook: https://angular.io/guide/lifecycle-hooks
  // The ngOnInit() is a lifecycle hook. Angular calls ngOnInit() shortly after creating a component
  // (*) Instead, call getHeroes() inside the 'ngOnInit' lifecycle hook and let Angular call ngOnInit() at an appropriate time
  //  after constructing a HeroesComponent instance.
  ngOnInit() {
    this.getHeroes();
  }

  // Create a method to retrieve the heroes from the service.
  // getHeroes(): void {
  //   this.heroes = this.heroService.getHeroes();
  // }

  getHeroes(): void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes);
  }

  // onSelect(hero: Hero): void {
  //   this.selectedHero = hero;
  //   this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}'`);
  // }

}
